// //This is an example of a statement
// // alert("Hello World");

// // //Variable
// // let name; //variable declaration
// // name = 'Zuitt';
// // alert(name);
// // name = 'Coding Bootcamp';
// // alert(name);

// // let phrase = "javsscript";
// // let phrase2 = 'hey man';

// // console.log(phrase2);
// // let myScore = 98.50;
// // console.log(myScore);
// // console.log(name);

// //Constant
// // const boilingPoint = 100;
// // console.log('Boiling point of water' + boilingPoint);

// // boilingPoint = 200;

// //Data Type in JS
// /*
// 1.String Example let name = 'Zuitt';
// 2.Numeric Example let score = 200.50;
// 3.Boolean | Two values, either True or False
// 	let isLegalAge = true;
// 	console.log(isLegalAge);
// 4.Undefined
// 5.Null
// */

// // let course = null;
// // console.log(course)


// //Functions

// function makeTriangle(){
// 	console.log('*');
// 	console.log('**');
// 	console.log('***');
// 	console.log('****');
// 	console.log('*****');
// }

// //invoke
// // makeTriangle();
// // makeTriangle();
// // makeTriangle();

// // function sayHello(name){
// // 	alert('Helow!' + name)
// // 	console.log('Hellow!' + name);
// // 	console.log('===end of code ===')
// // }


// // 	sayHello('shella'); //argument
// // 	sayHello('John');

// //Functions with multiple parameters
// function sayHi(lname, fname){
// 	console.log('Hi ' + lname + ', ' + fname);
// }

// // sayHi(`Starks`, `Tony`);

// function addNum(num1, num2){
// 	let x = num1 +num2
// 	console.log('the sum is: ' + x);
// // }

// // addNum(12,5);

// // function addTwoNums(x, y){
// // 	// console.log(x+y);
// // 	return x+y;
// // }

// let x = addTwoNums(5,8);

// console.log(x/5);
// console.log('The sum is ' + x);

//Mini-activity
//Create a function called doMath that takes in two arguments 

/*
Example:doMath(2,5)
Expected output 
The sum is: 7
The Product is: 10
The difference: -3 
The quotient is: 0.4 
*/

// function doMath(num1 , num2){
// 	console.log(num1 + num2)
// 	console.log(num1 * num2)
// 	console.log(num1 - num2)
// 	console.log(num1 / num2)
// }

// doMath(2,5);

let name = prompt('Enter your name');
console.log(name);